global function AutoStart_Init

void function AutoStart_Init()
{
	thread LaunchGame()
}

string function GetMap()
{
	string randomMap = GetRandomMap()

	bool random = GetCurrentPlaylistVarInt("RandomMap", 0) == 1
	if (!random)
		return GetCurrentPlaylistVarString("ChosenMap", randomMap)

	return randomMap
}

string function GetMode()
{
	return GetCurrentPlaylistVarString("ChosenMode", "tdm")
}

void function LaunchGame()
{
	bool inLobby = GetMapName() == "mp_lobby"
	bool enabled = GetCurrentPlaylistVarInt("AutoStart", 0) == 1

	if (inLobby && enabled)
	{
		wait 5.0

		SetCurrentPlaylist(GetMode())
		GameRules_ChangeMap(GetMap(), GetMode())
	}
}

string function GetRandomMap()
{
	array<string> map_list = []

	foreach (string map in maps)
	{
		if (map.find("_lf_") != null)
			continue
		if (map.find("coliseum") != null)
			continue

		map_list.append(map)
	}

	return map_list[RandomInt(map_list.len())]
}

array<string> maps = [
	"mp_angel_city",        	// Angel City
	"mp_black_water_canal", 	// Black Water Canal
	"mp_grave",             	// Boomtown
	"mp_colony02",          	// Colony
	"mp_complex3",          	// Complex
	"mp_crashsite3",        	// Crashsite
	"mp_drydock",           	// DryDock
	"mp_eden",              	// Eden
	"mp_thaw",              	// Exoplanet
	"mp_forwardbase_kodai", 	// Forward Base Kodai
	"mp_glitch",            	// Glitch
	"mp_homestead",         	// Homestead
	"mp_relic02",           	// Relic
	"mp_rise",              	// Rise
	"mp_wargames",          	// Wargames
	"mp_lf_deck",           	// Deck
	"mp_lf_meadow",         	// Meadow
	"mp_lf_stacks",         	// Stacks
	"mp_lf_township",       	// Township
	"mp_lf_traffic",        	// Traffic
	"mp_lf_uma",            	// UMA
	"mp_coliseum",          	// The Coliseum
	"mp_coliseum_column",   	// Pillars
]

array<string> modes = [
	"tdm",						// Skirmish
	"ps", 						// Pilots vs Pilots
	"gg", 						// Gungame
	"sns", 						// Sticks & Stones
	"tt", 						// Titan Tag
	"inf", 						// Infected
	"fastball", 				// Fastball
	"ctf_comp", 				// Competitive Capture the Flag
	"hs", 						// Hide and Seek
	"cp", 						// Hardpoint
	"lts", 						// Last Titan Standing
	"ctf", 						// Capture the Flag
	"ttdm", 					// Titan Brawl
	"turbo_ttdm", 				// Turbo Titan Brawl
	"attdm", 					// Aegis Titan Brawl
	"ffa", 						// Free for all
	"tffa", 					// Titan Free for All
	"fra", 						// Free Agents
	"coliseum", 				// Coliseum
	"lf", 						// Live Fire
	"rocket_lf", 				// Rocket Arena
	"mfd" 						// Marked for Death
]
