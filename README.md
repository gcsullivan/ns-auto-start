# Auto Start

This mod will automatically start the match for Northstar dedicated TF2 servers.

## Config

 * AutoStart
   - 0 or 1
 * RandomMap
   - 0 or 1
 * ChosenMap
   - Full name of a map, such as `mp_forwardbase_kodai`
 * ChosenMode
   - Full name of a mode, such as `lts`

## Usage (Examples)

To automatically start a game of LTS on a random map:

`+setplaylistvaroverrides "AutoStart 1 RandomMap 1 ChosenMode lts"`

To automatically start a game of Titan Brawl on Glitch:

`+setplaylistvaroverrides "AutoStart 1 ChosenMap mp_glitch ChosenMode ttdm"`

## Notes

This mod is only for use on dedicated servers.
